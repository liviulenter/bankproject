window.onload = function () {
  let user = { "name": "Liviu the Banker From Denmark", "bankBalance": 200, "payBalance": 0, "loanBalance": 0 }
  let noOfLoans = 0;
  let userNameElement = document.querySelector("#user-name")
  let bankBalanceElement = document.querySelector("#user-balance")
  let payBalanceElement = document.querySelector("#work-balance")
  let loanBalanceElement = document.querySelector("#loan-balance")
  let loanElement = document.querySelector("#loan-element")
  let alert = document.querySelector("#alert")

  let loanButton = document.querySelector("#loan-button")
  let repayButton = document.querySelector("#repay-button")
  let workButton = document.querySelector("#work-button")
  let bankButton = document.querySelector("#bank-button")
  let saveLoanButton = document.querySelector("#save-loan-button")
  let buyButton = document.querySelector("#buy-button")

  let amountInput = document.querySelector("#amount-input")
  let laptopsSelect = document.querySelector("#laptops-select")
  var modal = document.querySelector(".modal");
  var container = modal.querySelector(".container");

  userNameElement.innerHTML = user.name;
  bankBalanceElement.innerHTML = user.bankBalance;
  payBalanceElement.innerHTML = user.payBalance
  loanBalanceElement.innerHTML = user.loanBalance

  let laptops = JSON.parse(httpGetLaptops());
  populateSelectLaptops(laptops)

  buyButton.addEventListener("click", function(e){
    let index = laptopsSelect.value
    let message = ""
    let laptopPrice = laptops[index].price
    if(laptopPrice > user.bankBalance){
        message = "You do not have enough money in the bank to buy this laptop!"
        alert.classList.remove("hidden")
        alert.classList.add("alert-danger")
        alert.innerHTML = message
        return
    }
    user.bankBalance -= laptopPrice
    noOfLoans = 0
    message = "You are the owner of this laptop!"
    alert.classList.remove("hidden")
    alert.classList.add("alert-success")
    alert.innerHTML = message
  })

  loanButton.addEventListener("click", function () {
    $('#loan-modal').modal('show')
  });

  workButton.addEventListener("click", function(e){
    user.payBalance += 100
    payBalanceElement.innerHTML = user.payBalance
  })

  repayButton.addEventListener("click", function(e){
    
    if(user.payBalance > user.loanBalance){
      user.payBalance -= user.loanBalance
      user.loanBalance = 0
    }
    else{
      user.loanBalance -= user.payBalance
      user.payBalance = 0
    }
    
    payBalanceElement.innerHTML = user.payBalance
    loanBalanceElement.innerHTML = user.loanBalance
    noOfLoans = 0
    repayButton.classList.add("hidden")
  })

  bankButton.addEventListener("click", function(e){
    let amount = 90/100 * user.payBalance
    user.loanBalance += 10/100 * user.payBalance 
    user.bankBalance += amount
    user.payBalance  = 0;
    payBalanceElement.innerHTML = user.payBalance
    bankBalanceElement.innerHTML = user.bankBalance
    loanBalanceElement.innerHTML = user.loanBalance
  })


  saveLoanButton.addEventListener("click", function(e){
    let amount =  Number(amountInput.value)
    let errorMessage=""
    let errorElement = document.getElementById("error-message")
    if(amount > user.bankBalance*2 ){
      errorMessage = "You cannot get a loan more than double of your bank balance!"
      errorElement.innerHTML = errorMessage
      return
    }
    if(noOfLoans>0){
      errorMessage = "You cannot get more than one bank loan before buying a computer!"
      errorElement.innerHTML = errorMessage
      return 
    }
    user.bankBalance += amount
    noOfLoans++;
    bankBalanceElement.innerHTML = user.bankBalance

    user.loanBalance += amount
    loanElement.classList.remove("hidden")
  
    loanBalanceElement.innerHTML = user.loanBalance

    errorElement.innerHTML= ""
    amountInput.value = ""
    
    repayButton.classList.remove("hidden")
    $('#loan-modal').modal('hide')
  });

  document.querySelector(".modal").addEventListener("click", function (e) {
    if (e.target !== modal && e.target !== container) return;
    modal.classList.add("hidden");
  });


  let featuresElement = document.getElementById("features")
  let laptopTitle = document.getElementById("laptop-title")
  let laptopDescription = document.getElementById("laptop-description")
  let laptopPrice = document.getElementById("laptop-price")
  let featuresString = ""
  //populate with firdty laptop''s dspecs in features element
  for (const spec of laptops[0].specs) {
    console.log(spec)
    featuresString += spec + " "
  }
  featuresElement.innerHTML = featuresString
  laptopTitle.innerHTML = laptops[0].title
  laptopDescription.innerHTML = laptops[0].description
  laptopPrice.innerHTML = laptops[0].price + "DKK"

  //populate feastures element on chnage laptop selection
  laptopsSelect.addEventListener("change", function (event) {
    let laptopsIndex = event.target.value

    for (const spec of laptops[laptopsIndex].specs) {
      console.log(spec)
      featuresString += spec + " "
    }
    featuresElement.innerHTML = featuresString
    laptopTitle.innerHTML = laptops[laptopsIndex].title
    laptopDescription.innerHTML = laptops[laptopsIndex].description
    laptopPrice.innerHTML = laptops[laptopsIndex].price + " DKK"

  });
}

function httpGetLaptops() {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", "https://noroff-komputer-store-api.herokuapp.com/computers", false); // false for synchronous request
  xmlHttp.send();
  return xmlHttp.responseText;
}

function populateSelectLaptops(laptops) {
  console.log("populate")
  let index = 0
  let selectLaptops = document.querySelector("#laptops-select")
  for (const element of laptops) {
    console.log(element)
    var opt = document.createElement("option");
    opt.value = index;
    opt.innerHTML = element.title;
    selectLaptops.appendChild(opt);
    index++;
  }

}
function changeInfoOnSelect() {
  let laptopsSelect = document.getElementById("latpops-select")
  console.log(laptopsSelect)
  document.getElementById("features").innerHTML = "You selected: " + laptopsSelect.value;
}